Grade composition:

    written exam (up to 26 points): typically 3 exercises and few open questions on the course's topics (see template exams from past years here below).
    project (up to 8 points): project development using some tools which we will study at the hands-on activities; projects will be proposed during the course, usually within mid May; few rules on projects:
        project is not mandatory (you may decide not to do it, but in that case the maximum achievable grade is 26/30)
        projects can be developed in groups of maximum 2 people
        The following rule does apply on grading: assuming that a project is graded with 8 points (perfect project), if the project is submitted before Sept. 28, 2017, the obtained grade is 8 points; if the project is submitted after Sept. 28, 2017 but before February , 2018, the obtained grade is scaled down by half, that is 4 points; if a project is submitted after February, 2018, the obtained grade is 0.
    oral exam is my choice, not yours (aka, don't come asking for an oral exam because the answer is NO)

Other rules on grading and grade registration:

    to have a grade filed in your career you MUST be registered to an exam (don't ask me to register grades if your are not registered to an exam, because simply I canot do it)
    grades obtained  in a given exam (date) remain valid until the student submits a new exam
    grades can be registered in any exam date, provided that the you have regularly and correctly registered to that exam date

