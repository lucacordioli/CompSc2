interface Receive {
	event message_t* receive(message_t* msg, void* payload, uint8_t lencall);
}

interface Read<val_t>{
	command error_t read();
	event void readDone(error_t err, val_t val);
}
