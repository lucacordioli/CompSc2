# problema della scuola di vela

# numero di allievi non esperti 
param n >= 0;
set A := 1..n;

# numero di allievi esperti
param m >=0;
set E := 1..m;

# numero massimo di skipper disponibili
param s >= 0;
set S := 1..s;

param B{S};    # numero massimo di persone che possono essere affidate ad uno skipper per un corso di gruppo
param K;       # numero minimo di allievi esperti da accettare nella scuola;

param r >= 0;  # costo iscrizione al corso di base
param c >= 0;  # costo di iscrizione al corso per allievi esperti
param g >= 0;  # costo di uno skipper per costo di base
param l >= 0;  # costo di uno skipper per costo avanzato

var x{A, S} >= 0, binary;  # assegnamento dell'allievio i allo skipper j per corso di base
var y{A, S} >= 0, binary;  # assegnamento dell'allievio i allo skipper j per corso avanzato
var w{S}    >= 0, binary;  # vale 1 se lo skipper j e' assegnato a un corso di gruppo
var z{S}    >= 0, binary;  # vale 1 se lo skipper j e' assegnato a un corso avanzato

maximize profitto: 
	sum {i in A, j in S} r * x[i,j] + 
	sum {i in E, j in S} c * y[i,j] - 
	sum {j in S} (g * w[j] + l * z[j]);

subject to assegnamento_allievi_base {i in A}:
	sum {j in S} x[i,j] <= 1;

subject to assegnamento_allievi_avanzato {i in E}:
	sum {j in S} y[i,j] <= 1;

subject to max_capacita {j in S}:
	sum {i in A} x[i,j] <= B[j]*w[j];

subject to bigM {j in S}:
	sum {i in E} y[i,j] <= z[j];

subject to un_corso {j in S}:
	w[j] + z[j] <= 1;

subject to minimo_numero_esperti:
	sum {i in E, j in S} y[i,j] >= K;







