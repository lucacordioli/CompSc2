var x1 >=0;
var x2 >=0;

param p1 default 1;
param p2 default 1;
param b1 default 2;
param b2 default 4;

maximize obj:
	p1*x1 + p2*x2;

subject to const1:
	0.5*x1 + x2 <= b1;

subject to const2:
	2 * x1 + x2 <= b2;

