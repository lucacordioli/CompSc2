# SETS

set I;
param n;
set J := 1..n;

# PARAMS

param b{J};
param d{I,J};
param r{I};
param c{I};
param q{I};
param f{I};
param l{I};
param m{I};
param K;

