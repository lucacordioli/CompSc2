# SETS

set V;                       
set E within {V,V};
set A within {V,V} :=
   E union setof{(i,j) in E} (j,i);

# PARAMS

param s symbolic in V; #sorgente
param t symbolic in V; #destinazione
param k{(i,j) in A} >= 0, default if (j,i) in E then k[j,i]; #capacit�

#VAR

var x{(i,j) in A} >= 0; #flusso sull'arco (i,j)

#FUN

#massimizza il flusso netto su s
maximize flusso:
   sum{(s,j) in A} x[s,j] - sum{(i,s) in A} x[i,s];

#VINCOLI

subject to capacita{(i,j) in A}:
   x[i,j] <= k[i,j];

subject to conservazione_generale{i in V diff {s,t}}:
   sum{(i,j) in A} x[i,j] = sum{(j,i) in A} x[j,i];

#ridondante
#subject to conservazione_s_t:
#   sum{(s,j) in A} x[s,j] = sum{(j,t) in A} x[j,t];


