# SETS

set V;
set E within {V,V};
set A within {V,V} :=
   E union setof{(i,j) in E} (j,i);
set D;

# PARAMS

param s{D} symbolic in V;
param t{D} symbolic in V;
param k{(i,j) in A} >= 0, default if (j,i) in E then k[j,i];

#VAR
var x{A} >= 0;
 
#FUN

#??????????????????????

#VINCOLI

subject to capacita{(i,j) in A}:
   x[i,j] <= k[i,j];

subject to conservazione_generale{i in V diff {s,t}}:
   sum{(i,j) in A} x[i,j] = sum{(j,i) in A} x[j,i];



