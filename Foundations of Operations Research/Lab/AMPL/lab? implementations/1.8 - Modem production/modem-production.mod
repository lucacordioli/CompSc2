/*******************modem-production.mod***************/

/*****************Point 1****************/

param n;		#number of months in the planning horizon
set J:=1..n;	#set of months

param p;		#production capacity
param c;		#unit cost of production

param c2;		#unit cost for buying a product from company B
param m;		#max buyable units from the company B per month

param i;		#inventory cost for one unit per month

param initialInventory;		#initial number of unit stored at the beginning
param finalInventory;		#final number of unit that must be stored at the end

param l;		#minimum amount of units to be produced for a lot

param d{J};		#demand for the j-th month

var x{J} >= 0, integer;				#number of units produced in j-th month
var b{J} >= 0, integer;				#number of bought units from company B in j-th month
var s{J union {0}} >= 0, integer;	#number of units stored at the end of j-th month

var yA{J} binary;			#yA[j] = 1 if production line is opened in month j in company A

minimize totalCost:
	sum{j in J}( x[j]*c + b[j]*c2 + s[j]*i );

subject to initializationInventory:
	s[0] = initialInventory;

subject to finalizationInventory:
	s[n] = finalInventory;

subject to maxBuyableFromB{j in J}:
	b[j] <= m;
	
subject to productionCapacity{j in J}:
	x[j] <= yA[j]*p;

subject to minimumLotSize{j in J}:
	x[j] >= yA[j]*l;

subject to demand{j in J}:
	x[j] + b[j] + s[j-1] >= d[j];

subject to balancing{j in J}:
	s[j] = x[j] + b[j] + s[j - 1] - d[j];

/***************Point 2*****************/
param fA;		#fixed charge for each month that company A uses for production
param fB;		#fixed charge for each month that company B uses for production

var yB{J} binary;#yA[j] = 1 if production line is opened in month j in company A

minimize totalCost2:
	sum{j in J}( x[j]*c + b[j]*c2 + s[j]*i + yA[j]*fA + yB[j]*fB );

subject to activationOfCompanyB{j in J}:
	b[j] <= yB[j]*m;

data;

param n:=4;
param p:=2500;
param c:=7;
param c2:=9;
param m:=700;
param i:=1;
param initialInventory:=70;
param finalInventory:=300;
param l:=80;
param d:=
	1	2600,
	2	3000,
	3	2350,
	4	2700;

param fA := 1000;
param fB := 500;
