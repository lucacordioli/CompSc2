Dare la definizione di densità spettrale di un processo stocastico stazionario ed elencarne le principali proprietà.
4. Dare la definizione di densità spettrale di un processo stocastico stazionario ed elencarne le principali proprietà.

Dare la definizione di rappresentazione canonica di un processo ARMA, elencandone le proprietà. Si discuta inoltre la possibilità di ottenere sempre una rappresentazione canonica di un qualunque processo ARMA

5. Si consideri un processo ARMA data in rappresentazione canonica. Spiegare perché la varianza dell’errore di predizione ad 1 passo coincide con la varianza del rumore bianco che genera il processo.

Si dimostri che, se il sistema di generazione dei dati S di un processo ARMA stabile appartiene alla famiglia dei
modelli M( θ ), e la parametrizzazione “vera” del modello è θ 0 , allora un metodo di identificazione a minimizzazione
dell’errore di predizione stima un vettore dei parametri che asintoticamente converge a θ 0 (nell’ipotesi che il punto di minimo della cifra di merito sia unico).

5. Si scrivano le espressioni analitiche dei criteri FPE, AIC e MDL per la scelta della complessità dei modelli. Si commentino brevemente le differenze fra i tre metodi.

5. Si supponga di conoscere N campioni di una realizzazione di un processo stocastico stazionario ε(t). Si descriva con precisione i passi da effettuare su questa sequenza di dati per fare una verifica di bianchezza, usando il test di Anderson.

5. Dare la definizione di spettro di un processo stocastico stazionario ed elencarne le principali proprietà.

4. Si supponga di conoscere N campioni di una realizzazione di un processo stocastico stazionario y(t): S: {y(1),y(2),y(3),...,y(N)}.
Si scriva l’espressione di:
- unostimatorecampionariodelvaloreattesodiy.
- unostimatorecampionariodellafunzionedicovarianzadiy.
Si discuta la correttezza e la consistenza dei due stimatori.


Si definisca il concetto di segnale stazionario persistentemente eccitante di ordine n. Si spieghi perche’ un rumore bianco è persistentemente eccitante di ordine infinito.

Si ricavi l’espressione generale del predittore ottimo dai dati a k passi in avanti di un modello ARMAX descritto dalla equazione:

Dare la definizione di segnale persistentemente eccitante di ordine n . Spiegare poi, brevemente, la rilevanza di questa nozione per la stima di parametri di modelli ARX.


Si traccino le linee guida per la scelta della complessità di un modello nell’ambito della teoria dell’identificazione

Si consideri un processo ARMA dato in rappresentazione canonica. Spiegare perché la varianza dell'errore di predizione ad 1 passo coincide con la varianza del rumore bianco che genera il processo.

Dato un processo stazionario y ( t ) si consideri il corrispondente predittore a k passi f(t\t - k).
Sia A| = Var[y(t) - f(t\t - k)] la varianza dell'errore di predizione. Si dica a che valore tende Al quando I'orizzonte predittivo tende all'infinito.

Si definisca il concetto di segnale stazionario persistentemente eccitante di ordine n. Si spieghi perche’ un rumore bianco è persistentemente eccitante di ordine infinito.

Dire cosa si intende per rappresentazione MA(∞) di un processo ARMA. Ricavare poi la rappresentazione MA(∞) del processo AR(1): y(t) = ay(t −1)+e(t), e(t) ≈WN(0,λ2 )

Si enunci il criterio FPE per la scelta della complessita’ dei modelli e si spieghi come utilizzarlo (non si chiede il ricavo di FPE).

Dare la definizione di funzione di covarianza γy(t1, t2) di un processo stocastico y(t) (in generale non stazionario). Elencare poi le principali proprieta’ di tale funzione nel caso particolare in cui y(t) sia stazionario.

Si diano le definizioni di valore atteso e di funzione di covarianza per un generico processo
stocastico. Si mostrino poi le principali proprieta’ di un processo stocastico stazionario.

Spiegare in che cosa consiste il metodo della cross-validazione per la scelta della complessità del modello

Spiegare che cosa si intende per filtro passatutto

Akaike

Dire che cosa si intende per rappresentazione canonica di un processo stocastico stazionario a spettro razionale.

Dato un processo ARMA, si spieghi in modo conciso ma rigoroso perchè l'errore di predizione ottimo a n passi è assimilabile ad un processo MA.

Stimare spettro di un processo stazionario dai campioni di una sua rilevazione temporale (realizzazione)