clear
clc
close all

n_states = 3;
n_actions = 3;
gamma = 0.9;

%% RL Solution

allowed_actions = [1 1 0; 1 0 1; 1 0 0];

s = randi(3);
Q = zeros(n_states,n_actions);
M = 3000;
m = 1;

policy = @eps_greedy;

a = policy(s, allowed_actions, Q, 1);

close all;
figure();

Q_hist = zeros(M,5);

while m < M
    alpha = (1 - m/M)^10;
    eps = (1 - m/M);
	%eps = 0.1;

    % Environment
    [s_prime, inst_rew] = transition_model(s, a);
    
    % Eps-greedy
    a_prime = policy(s_prime, allowed_actions, Q, eps);
    
    % SARSA update
    Q(s,a) = Q(s,a) + alpha * (inst_rew +...
        gamma * Q(s_prime,a_prime) - Q(s,a));
    s = s_prime;
    a = a_prime;

    m = m + 1;
    
    if mod(m,M/50) == 0
        clf();
        bar3(Q');
        xlabel('States');
        ylabel('Actions');
        zlim([-300 250]);
        title(['m = ' num2str(m)])
        drawnow;
    end
    
    Q_hist(m,:) = Q([1 2 3 4 8]);
end

figure()
plot(1:M, Q_hist');
legend({'Q(1,d)' 'Q(2,d)' 'Q(3,d)' 'Q(2,so)' 'Q(3,cm)'})

load final.mat
[Q Q_final]