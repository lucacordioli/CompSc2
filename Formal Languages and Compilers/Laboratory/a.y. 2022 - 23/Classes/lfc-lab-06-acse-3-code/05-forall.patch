diff --git a/acse/Acse.lex b/acse/Acse.lex
index 35f9b73..02eeaf7 100644
--- a/acse/Acse.lex
+++ b/acse/Acse.lex
@@ -93,6 +93,9 @@ ID       [a-zA-Z_][a-zA-Z0-9_]*
 "return"          { return RETURN; }
 "read"            { return READ; }
 "write"           { return WRITE; }
+"forall"          { return FORALL; }
+"to"              { return TO; }
+"downto"          { return DOWNTO; }
 
 {ID}              { yylval.svalue=strdup(yytext); return IDENTIFIER; }
 {DIGIT}+          { yylval.intval = atoi( yytext );
diff --git a/acse/Acse.y b/acse/Acse.y
index 0636dc6..4940dc5 100644
--- a/acse/Acse.y
+++ b/acse/Acse.y
@@ -109,6 +109,7 @@ extern void yyerror(const char*);
    t_list *list;
    t_axe_label *label;
    t_while_statement while_stmt;
+   t_forall_statement forall_stmt;
 } 
 /*=========================================================================
                                TOKENS 
@@ -125,6 +126,7 @@ extern void yyerror(const char*);
 %token RETURN
 %token READ
 %token WRITE
+%token TO DOWNTO
 
 %token <label> DO
 %token <while_stmt> WHILE
@@ -133,11 +135,13 @@ extern void yyerror(const char*);
 %token <intval> TYPE
 %token <svalue> IDENTIFIER
 %token <intval> NUMBER
+%token <forall_stmt> FORALL
 
 %type <expr> exp
 %type <decl> declaration
 %type <list> declaration_list
 %type <label> if_stmt
+%type <intval> forall_evolution
 
 /*=========================================================================
                           OPERATOR PRECEDENCES
@@ -254,12 +258,74 @@ control_statement : if_statement         { /* does nothing */ }
             | while_statement            { /* does nothing */ }
             | do_while_statement SEMI    { /* does nothing */ }
             | return_statement SEMI      { /* does nothing */ }
+            | forall_statement           { /* does nothing */ }
 ;
 
 read_write_statement : read_statement  { /* does nothing */ }
                      | write_statement { /* does nothing */ }
 ;
 
+forall_evolution : TO     { $$ = 0; }
+                 | DOWNTO { $$ = 1; }
+;
+
+forall_statement:
+   FORALL LPAR IDENTIFIER ASSIGN exp forall_evolution
+   {
+      /* Check that the identifier is not associated to an array */
+      t_axe_variable *var = getVariable(program, $3);
+      if (var->isArray)
+         notifyError(AXE_INVALID_VARIABLE);
+
+      /* Generate code to initialize the variable */
+      int vreg = get_symbol_location(program, $3, 0);
+      if ($5.expression_type == IMMEDIATE)
+         gen_addi_instruction(program, vreg, REG_0, $5.value);
+      else
+         gen_add_instruction(program, vreg, REG_0, $5.value, CG_DIRECT_ALL);
+
+      /* Create a label pointing to the condition check */
+      $1.label_condition = assignNewLabel(program);
+   }
+   exp RPAR
+   {
+      /* Generate code to check if the variable is equal to the final value */
+      int vreg = get_symbol_location(program, $3, 0);
+      t_axe_expression iv = create_expression(vreg, REGISTER);
+      /* This check will ALWAYS be done at RUNTIME (by generating code) because
+       * one of the two terms of the expression is of type REGISTER.
+       * Remember: this is NOT true in general! */ 
+      t_axe_expression cmp = handle_binary_comparison(program, iv, $8, _NOTEQ_);
+
+      /* Generate code to update the PSW flags */
+      if (cmp.expression_type == REGISTER)
+         gen_andb_instruction(program, cmp.value, cmp.value, cmp.value, CG_DIRECT_ALL);
+      else
+         assert(0); // this case is impossible for the reason explained above
+      /* Generate the branch that breaks the loop when the variable reached the
+       * final value */
+      $1.label_end = newLabel(program);
+      gen_beq_instruction(program, $1.label_end, 0);
+   }
+   code_block
+   {
+      /* Generate the code for incrementing or decrementing the variable */
+      int vreg = get_symbol_location(program, $3, 0);
+      if ($6 == 0) // to
+         gen_addi_instruction(program, vreg, vreg, 1);
+      else // downto
+         gen_subi_instruction(program, vreg, vreg, 1);
+      /* Generate the back-edge of the loop */
+      gen_bt_instruction(program, $1.label_condition, 0);
+
+      /* Generate the label for breaking the loop */
+      assignLabel(program, $1.label_end);
+      
+      /* Free the identifier string (dynamically allocated in Acse.lex) */
+      free($3);
+   }
+;
+
 assign_statement : IDENTIFIER LSQUARE exp RSQUARE ASSIGN exp
             {
                /* Notify to `program' that the value $6
diff --git a/acse/axe_struct.h b/acse/axe_struct.h
index 00cb86f..a0fed99 100644
--- a/acse/axe_struct.h
+++ b/acse/axe_struct.h
@@ -110,6 +110,12 @@ typedef struct t_while_statement
                                     * that follows the while construct */
 } t_while_statement;
 
+typedef struct t_forall_statement
+{
+   t_axe_label *label_condition;
+   t_axe_label *label_end;
+} t_forall_statement;
+
 /* create a label */
 extern t_axe_label *alloc_label(int value);
 
diff --git a/tests/forall/downto.src b/tests/forall/downto.src
new file mode 100644
index 0000000..ff9a59e
--- /dev/null
+++ b/tests/forall/downto.src
@@ -0,0 +1,10 @@
+int a;
+int b;
+int i;
+
+read(a);
+read(b);
+
+forall (i = a + 1 downto b-1) {
+   write(2 * i);
+}
diff --git a/tests/forall/to.src b/tests/forall/to.src
new file mode 100644
index 0000000..5d908bd
--- /dev/null
+++ b/tests/forall/to.src
@@ -0,0 +1,10 @@
+int a;
+int b;
+int i;
+
+read(a);
+read(b);
+
+forall (i = a + 1 to b-1) {
+   write(2 * i);
+}
