/* Prologue */

%{
#include <stdio.h>

int yylex(void);
void yyerror(const char *);
%}

%union {
  int value;
}

%token <value> NUMBER
%token NEWLINE PLUS
%token MINUS MUL DIV
%type <value> exp

/* Declarations */
%%

/*
 * beginnning of grammar rules
 */

program:
  lines NEWLINE //this is the whole program. Then it expands.
  {
    printf("bye!\n");
    YYACCEPT;
  } // this is like a terminal.
;

lines:
  lines line //each line could be expanded into two non terminals
  | %empty
  ;

line: 
  exp NEWLINE
  {
    printf("result = %d\n", $1);
  }
;

exp:
  NUMBER
  {
    $$ = $1; //update %union value
  }
  | exp exp PLUS
  {
    $$ = $1 + $2;
  }
  | exp exp MINUS
  {
    $$ = $1 - $2;
  }
  | exp exp MUL
  {
    $$ = $1 * $2;
  }
  | exp exp DIV
  {
    $$ = $1 / $2;
  }
;

/*
 * End of grammar  rules
 */

%%

/*
 * Compiler code
 */

void yyerror(const char *msg)
{
  fprintf(stderr, "%s\n", msg);
}


int main(int argc, char *argv[])
{
  yyparse();
  return 0;
}

