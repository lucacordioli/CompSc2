diff --git a/acse/Acse.lex b/acse/Acse.lex
index 35f9b73..bd35ee0 100644
--- a/acse/Acse.lex
+++ b/acse/Acse.lex
@@ -93,6 +93,8 @@ ID       [a-zA-Z_][a-zA-Z0-9_]*
 "return"          { return RETURN; }
 "read"            { return READ; }
 "write"           { return WRITE; }
+"random"          { return RANDOM; }
+"randomize"       { return RANDOMIZE; }
 
 {ID}              { yylval.svalue=strdup(yytext); return IDENTIFIER; }
 {DIGIT}+          { yylval.intval = atoi( yytext );
diff --git a/acse/Acse.y b/acse/Acse.y
index 0636dc6..c1909a2 100644
--- a/acse/Acse.y
+++ b/acse/Acse.y
@@ -90,6 +90,9 @@ t_reg_allocator *RA;       /* Register allocator. It implements the "Linear
 
 t_io_infos *file_infos;    /* input and output files used by the compiler */
 
+/* Register that at runtime will contain the last pseudo-random number
+ * generated. */
+int r_random;
 
 extern int yylex(void);
 extern void yyerror(const char*);
@@ -125,6 +128,7 @@ extern void yyerror(const char*);
 %token RETURN
 %token READ
 %token WRITE
+%token RANDOM RANDOMIZE
 
 %token <label> DO
 %token <while_stmt> WHILE
@@ -167,7 +171,14 @@ extern void yyerror(const char*);
       2. A list of instructions. (at least one instruction!).
  * When the rule associated with the non-terminal `program' is executed,
  * the parser notifies it to the `program' singleton instance. */
-program  : var_declarations statements EOF_TOK
+program  : var_declarations 
+         {
+            /* Reserve the register used by random() and randomize().
+             * Before the beginning of the program, generate code to initialize
+             * it with its initial value, which is 12345. */
+             r_random = gen_load_immediate(program, 12345);
+         }
+         statements EOF_TOK
          {
             /* Notify the end of the program. Once called
              * the function `set_end_Program' - if necessary -
@@ -247,6 +258,7 @@ statements  : statements statement       { /* does nothing */ }
 statement   : assign_statement SEMI      { /* does nothing */ }
             | control_statement          { /* does nothing */ }
             | read_write_statement SEMI  { /* does nothing */ }
+            | randomize_statement SEMI
             | SEMI            { gen_nop_instruction(program); }
 ;
 
@@ -260,6 +272,18 @@ read_write_statement : read_statement  { /* does nothing */ }
                      | write_statement { /* does nothing */ }
 ;
 
+randomize_statement: RANDOMIZE LPAR exp RPAR
+   {
+      /* Generate code to assign the value of the expression to the
+       * register identified by r_random. */
+      if ($3.expression_type == IMMEDIATE) {
+         gen_move_immediate(program, r_random, $3.value);
+      } else {
+         gen_add_instruction(program, r_random, REG_0, $3.value, CG_DIRECT_ALL);
+      }
+   }
+;
+
 assign_statement : IDENTIFIER LSQUARE exp RSQUARE ASSIGN exp
             {
                /* Notify to `program' that the value $6
@@ -458,7 +482,22 @@ write_statement : WRITE LPAR exp RPAR
             }
 ;
 
-exp: NUMBER      { $$ = create_expression ($1, IMMEDIATE); }
+exp: RANDOM LPAR RPAR
+   {
+      /* Reserve a new register for the result of the expression. We could
+       * re-use the r_random register, but that would produce incorrect results
+       * in expressions like `random() * random()'. */
+      int r_res = getNewRegister(program);
+      /* Generate code to advance the pseudo-random number generator using
+       * the LCG formula. */
+      gen_muli_instruction(program, r_res, r_random, 1664525);
+      gen_addi_instruction(program, r_res, r_res, 1013904223);
+      /* Generate code to update the register identified by r_random. */
+      gen_add_instruction(program, r_random, REG_0, r_res, CG_DIRECT_ALL);
+      /* The result of the expression is the number just computed. */
+      $$ = create_expression(r_res, REGISTER);
+   }
+   | NUMBER      { $$ = create_expression ($1, IMMEDIATE); }
    | IDENTIFIER  {
                      int location;
    
diff --git a/tests/rand/rand.src b/tests/rand/rand.src
new file mode 100644
index 0000000..1af4d53
--- /dev/null
+++ b/tests/rand/rand.src
@@ -0,0 +1,12 @@
+int a, i;
+
+i = 0;
+while (i < 3) {
+   write(random());      // 87628868, 71072467, -1962130921
+   i = i + 1;
+}
+
+randomize(12300 + 45);
+write(random());         // writes 87628868
+a = random() / 1000 + 7;
+write(a);                // writes 71072467 / 1000 + 7 = 71079
